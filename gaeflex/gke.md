set environment variable:
export PROJECT_ID=<<project id>>

build the package:
mvn clean package

Dockerfile contents:
```
FROM openjdk:8
COPY target/gaeflex-0.0.1-SNAPSHOT.jar /app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "/app.jar"]
```

build the container image in cloud shell:
```
docker build -t gcr.io/$PROJECT_ID/gaeflex:blue .
```

run the container in cloud shell to test:
docker run -ti --rm -p 8080:8080 gcr.io/$PROJECT_ID/gaeflex:blue

push the image to the gcr repo:
```
gcloud docker -- push gcr.io/$PROJECT_ID/gaeflex:blue
```

create gke cluster:
```
gcloud container clusters create flex-cluster \
  --num-nodes 4 \
  --machine-type n1-standard-1 \
  --zone us-central1-c
```  

Connect to GKE cluster if it is already running:
```
gcloud container clusters get-credentials flex-cluster
```

run the app on gke:
```
kubectl run flex-java \
  --image=gcr.io/$PROJECT_ID/gaeflex:blue \
  --port=8080
```
deploy directly from pod file:  
```
kubectl apply -f pod.yaml
```
