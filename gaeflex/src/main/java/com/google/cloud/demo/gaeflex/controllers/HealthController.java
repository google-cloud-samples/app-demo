package com.google.cloud.demo.gaeflex.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created in app-demo on 6/25/18.
 */
@Controller
@RequestMapping(value = "/health", produces = "application/json")
public class HealthController {

  @RequestMapping(value = "/liveness_check", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity getLivenessCheck() {
    return new ResponseEntity(HttpStatus.OK);
  }

  @RequestMapping(value = "/readiness_check", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity getReadinessCheck() {
    return new ResponseEntity(HttpStatus.OK);
  }
}
