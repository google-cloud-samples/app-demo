package com.google.cloud.demo.gaeflex.controllers;

import com.google.cloud.demo.gaeflex.config.AppConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created in app-demo on 6/26/18.
 */
@Controller
@RequestMapping(value = "/demo", produces = "application/json")
public class DemoController {

  private final AppConfig appConfig;

  public DemoController(AppConfig appConfig) {
    this.appConfig = appConfig;
  }


  @RequestMapping(value = "/time", method = RequestMethod.GET)
  @ResponseBody
  public Map<String,String> getTime() {
    Map<String,String> map = new HashMap<>();
    Date date = new Date();
    map.put("date", date.toString());
    map.put("time", Long.toString(date.getTime()));

    return map;
  }

  @RequestMapping(value = "/echo", method = RequestMethod.GET)
  @ResponseBody
  public Map<String,Object> getEcho(HttpServletRequest request) {
    Map<String,Object> map = new HashMap<>();
    Map<String,String> headerMap = new HashMap<>();
    Enumeration<String> headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      headerMap.put(headerName, request.getHeader(headerName));
    }
    map.put("headers", headerMap);


    return map;
  }

  @RequestMapping(value = "/crash", method = RequestMethod.GET)
  @ResponseBody
  public Map<String,String> doCrash() {
    Map<String,String> map = new HashMap<>();
    System.exit(100);
    return map;
  }

  @RequestMapping(value = "/process", method = RequestMethod.GET)
  @ResponseBody
  public Map<String,String> doProcess() {
    long startTime = System.currentTimeMillis();
    Map<String,String> map = new HashMap<>();
    BigInteger bigInteger = new BigInteger(""+System.currentTimeMillis());
    BigInteger bigInteger1 = new BigInteger("0");
    for (int i = 0; i < 10000000; i++) {
      bigInteger1 = bigInteger.multiply(bigInteger.add(new BigInteger(""+i)));
    }
    System.out.println("bigInteger1 = " + bigInteger1);
    map.put("result", bigInteger1.toString());
    long endTime = System.currentTimeMillis();
    long timeTaken = endTime - startTime;
    map.put("startTime", ""+startTime);
    map.put("endTime", ""+endTime);
    map.put("timeTaken",""+timeTaken);
    return map;
  }




}
