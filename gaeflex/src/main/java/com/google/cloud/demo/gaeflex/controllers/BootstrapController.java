package com.google.cloud.demo.gaeflex.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created in app-demo on 7/6/18.
 */
@Controller
@RequestMapping(value = "/bootstrap")
public class BootstrapController {

  @RequestMapping("/init")
  @ResponseBody
  public ResponseEntity getBootstrap(HttpServletRequest request, HttpServletResponse response) {
    response.addCookie(new Cookie("test", "test"));
    return new ResponseEntity(HttpStatus.OK);
  }
}
